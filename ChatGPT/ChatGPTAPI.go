package ChatGPT

import (
	"context"
	"fmt"
	openai "github.com/sashabaranov/go-openai"
)

const apiKey = "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"

func Callgpt(q string) string {
	client := openai.NewClient(apiKey)
	resp, err := client.CreateChatCompletion(
		context.Background(),
		openai.ChatCompletionRequest{
			Model: openai.GPT3Dot5Turbo,
			Messages: []openai.ChatCompletionMessage{
				{
					Role:    openai.ChatMessageRoleUser,
					Content: q,
				},
			},
		},
	)

	if err != nil {
		fmt.Printf("ChatCompletion error: %v\n", err)
		return "接口调用失败"
	}
	return resp.Choices[0].Message.Content

}
